import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Grid, Row, Col } from 'react-bootstrap';

const mapStateToProps = (state, ownProps) => {
  return { 
    currentURL: ownProps.location.pathname,
    auth : state.auth, };
};

class Homepage extends Component {
  constructor() {
    super(); 
  }

  render() {
   
    return (
      <div className='content'>
        <Grid>
          <Row>
            <Col md={12}>
              <h2 style={{textAlign: 'center'}}> Treedots </h2>
              <p style={{textAlign: 'center', paddingBottom: '20px'}}> Homepage </p>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

Homepage.propTypes = {
  history : PropTypes.object.isRequired,
  dispatch : PropTypes.func.isRequired,
};
 
const HomepageComponent = connect(mapStateToProps)(Homepage);
export default HomepageComponent;