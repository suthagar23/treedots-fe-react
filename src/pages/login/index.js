import React, { Component } from 'react';
import { Grid, Row, Col, FormGroup, ControlLabel, FormControl, Alert } from 'react-bootstrap';
import Container from '../../components/Container'; 
import Button from '../../components/CustomButton';
import { validateUserLoginForm } from './action';
import { authenticateUser } from '../../external/auth';
import { connect } from 'react-redux';
import * as constants from './constants';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom'; 
import { DeleteAuthCookie, GetAuthCookie } from '../../utils/cookie';
import {PATH_HOMEPAGE } from '../../utils/constants';

const mapStateToProps = (state, ownProps) => {
  return { auth: state.auth };
};

const mapDispatchToProps = (dispatch) => {
  return {
    authenticateUser: (userLoginData) => dispatch(authenticateUser(userLoginData)),
    validateUserLoginForm: userLoginData => dispatch(validateUserLoginForm(userLoginData))
  };
};

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  return Object.assign({}, ownProps, {
    redux: {
      state: stateProps,
      actions: dispatchProps
    }
  });
};


class Login extends Component {
  constructor() {
    super();
    this.state = {
      email: '',
      password: '',
      ValidationIssues: undefined
    };
    this.handleChange = this.handleChange.bind(this);
    this.submitForm = this.submitForm.bind(this);
  }
  
  componentDidMount() {  
    const { history } = this.props;
    const {isLogedIn, userInfo} = GetAuthCookie() || {};
    if (typeof isLogedIn !== 'undefined' && typeof userInfo !== 'undefined') {
      history.push(PATH_HOMEPAGE);
    }
    else {
      DeleteAuthCookie();
    }
  }
 
  handleChange(event) {
    this.setState(Object.assign({}, this.state, {
      [event.target.name]: event.target.value,
      ValidationIssues: {
        email: false,
        password: false
      }}));
  }

  ChangeValidatorStates(isemailCorrect, isPasswordCorrect){
    this.setState(Object.assign({}, this.state, {
      ValidationIssues: {
        email: isemailCorrect,
        password: isPasswordCorrect
      }}));
  }

  submitForm(e) {
    e.preventDefault();
    const { redux } = this.props;this.setState(Object.assign({}, this.state, {
      ...this.state,
      ValidationIssues: {
        searchFieldValue: true
      }
    }));
    const {validator, email, password} = this.state;
    const payload = {
      type: "EMAIL",
      loginKey: email,
      password
    }
    const validated = redux.actions.validateUserLoginForm(payload);
    if (typeof validated !== 'undefined') {
      switch (validated.type) {
      case constants.AUTHENTICATE:
        this.ChangeValidatorStates(false, false);
        this.props.redux.actions.authenticateUser(payload);
      case constants.USERNAME_PASSWORD_REQUIRED || constants.LOGIN_REQUIRED:
        this.ChangeValidatorStates(true, true);
      case constants.USERNAME_REQUIRED:
        this.ChangeValidatorStates(true, false);
      case constants.PASSWORD_REQUIRED:
        this.ChangeValidatorStates(false, true);
      default:
        this.ChangeValidatorStates(false, false);
      }
    }
  }
    
  getValidationState(fieldName) {
    if (fieldName && typeof this.state.ValidationIssues !== 'undefined') {
      if (this.state[fieldName]) return null;
      if (this.state.ValidationIssues[fieldName]) return 'error';
      return 'success';
    }
    return null;
  }



  render() {
    const loginCardStyle = {
      float: 'none',
      margin: '0 auto'
    };
    
    const showAlerts = (alertTyle, message) => {
      return (<Alert bsStyle={alertTyle}> { message }</Alert>);
    };

    const UpdateLoginStatus = () => {
      const state_AuthObject = this.props.redux.state.auth;
      if (typeof state_AuthObject.error !== 'undefined') {
        return showAlerts('danger', ( state_AuthObject.error.message|| 'Unknown error'));
      }
      else if (typeof state_AuthObject.success !== 'undefined') {
        const { user } = state_AuthObject;
        if (user) {
          return (
            <div>
              <Redirect to={PATH_HOMEPAGE} /> 
              {showAlerts('success', ( state_AuthObject.success.message|| 'Success'))}
            </div>);
        }
        else {
          return showAlerts('danger', 'Unknown error, Please try again');
        }

      }
      return (null);
    };

    return (
      <div className="content">
        <Grid fluid>
          <Row>
            <Col md={5} style={loginCardStyle}>
              <Container
                title="Login with Credentials"
                content={
                  <form className="form" onSubmit={ (e) => this.submitForm(e) }>
                     
                    <UpdateLoginStatus />

                    <FormGroup controlId="email" validationState={this.getValidationState('email')}>
                      <ControlLabel>Email</ControlLabel>
                      <FormControl
                        type="text"
                        name="email"
                        value={this.state.value}
                        placeholder="Enter your email"
                        onChange={this.handleChange}
                      />                
                    </FormGroup>
                    
                    <FormGroup controlId="password" validationState={this.getValidationState('password')}>
                      <ControlLabel>Password</ControlLabel>
                      <FormControl
                        type="password"
                        name="password"
                        value={this.state.value}
                        placeholder="Enter your password"
                        onChange={this.handleChange}
                      />
                    </FormGroup>
                
                    <Button bsStyle="info" pullRight fill type="submit">
                      Login
                    </Button>
                    <div className="clearfix" />
                  </form>
                }
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

Login.propTypes = {
  redux: PropTypes.object.isRequired,
  redux: PropTypes.shape({
    actions: PropTypes.object.isRequired,
    actions: PropTypes.shape({
      authenticateUser: PropTypes.func.isRequired,
      validateUserLoginForm:  PropTypes.func.isRequired,
    }),
    state: PropTypes.object.isRequired,
    state: PropTypes.shape({
      auth: PropTypes.object.isRequired,
      auth: PropTypes.shape({
        logedIn: PropTypes.bool.isRequired
      })
    })
  }),
  history : PropTypes.object.isRequired,
};

const LoginComponent = connect(mapStateToProps,mapDispatchToProps, mergeProps)(Login);
export default LoginComponent;
