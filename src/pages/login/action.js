
import * as constants from './constants';

export function validateUserLoginForm(payload) {
  const { loginKey, password } = payload;
  if (!loginKey && !password) {
    return { type: constants.USERNAME_PASSWORD_REQUIRED, payload };
  }
  if (!loginKey) {
    return { type: constants.USERNAME_REQUIRED, payload };
  }
  if (!password) {
    return { type: constants.PASSWORD_REQUIRED, payload };
  }
  return { type: constants.AUTHENTICATE, payload };
}
