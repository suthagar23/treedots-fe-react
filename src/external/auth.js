import { fetchPost } from '../utils/restApi';
import * as constants from '../pages/login/constants';
import { CreateAuthCookie, DeleteAuthCookie } from '../utils/cookie';

export function authenticateUser(payload) {
    return (dispatch) => {
      return fetchPost('/auth/login', payload)
        .then((res) => {
          const resStatusCode = (res || {}).status;
          if (resStatusCode === 200) {
            const { user: userInfo, token } = res.data || {};
            if (userInfo) {
              CreateAuthCookie({ isLogedIn : true, userInfo: userInfo, authToken: token });
              dispatch({ type: constants.AUTHENTICATEION_SUCCESS, payload: res.data });
            }
            else {
              // if auth object doesn't contain user object
              DeleteAuthCookie();
              dispatch({ type: constants.AUTHENTICATEION_RESPONSE_ERROR, payload: res.data });
            }
          } else {
            dispatch({ type: constants.AUTHENTICATEION_ERROR, payload: { error: res.error, status: resStatusCode }});
          }
        })
        .catch((err) => {
          dispatch({ type: constants.AUTHENTICATEION_ERROR, payload: err });
        });
    };
  }
  