import React, { Component } from 'react';
import { NavItem, Nav } from 'react-bootstrap';
import { connect } from 'react-redux';  
import PropTypes from 'prop-types';
import { DeleteAuthCookie } from '../../../utils/cookie';

const mapStateToProps = (state, ownProps) => {
  return {auth: state.auth, };
};

class HeaderLinks extends Component {
  constructor(props) {
    super(props); 
  }

  handleLogOut() {
    DeleteAuthCookie();
  }

  render() { 
    return (
      <div>
        <Nav pullRight>
          <NavItem eventKey={3} onClick={this.handleLogOut} href="#/logout">
            Log out
          </NavItem>
        </Nav>
      </div>
    );
  }
}

HeaderLinks.propTypes = {
  dispatch: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
};

const HeaderLinksComponetn = connect(mapStateToProps)(HeaderLinks);
export default HeaderLinksComponetn;
