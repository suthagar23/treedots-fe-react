import React, { Component } from 'react';
import { Nav, NavItem } from 'react-bootstrap';

class LoginLinks extends Component {
    render() { 
      return (
        <div>
          <Nav pullRight>
            <NavItem eventKey={1} href="#/auth">
              Log in
            </NavItem>
          </Nav>
        </div>
      );
    }
  }

export default LoginLinks;