import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom'; 
import Header from './Header';
import Footer from './Footer'; 
import dashboardRoutes from '../../routes/dashboard.js';
import { connect } from 'react-redux'; 
import styles from './style.css';

const mapStateToProps = (state, ownProps) => {
  return { };
};

class Layout extends Component {
  constructor(props) {
    super(props);
    this.mainPanelRef= React.createRef(); 
    this.state = { };
  }

  render() {
     
    return (
      <div className="wrapper" >
        <div id="main-panel" className="main-panel" ref={this.mainPanelRef}>
          <Header {...this.props} />
          <Switch>
            {dashboardRoutes.map((prop, key) => {
              if (prop.redirect)
                return <Redirect from={prop.path} to={prop.to} key={key}  />;
              return (
                // <Route path={prop.path} component={prop.component}  key={key} cookies={ prop.cookies}/>
                <Route path={prop.path} key={key} render={(props) => (<prop.component {...this.props} />) }/>
              );
            })}
          </Switch>
          <Footer {...this.props} />
        </div>
      </div>
    );
  }
}

const LayoutComponent = connect(mapStateToProps)(Layout);
export default LayoutComponent;
