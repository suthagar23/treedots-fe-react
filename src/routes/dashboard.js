
import Homepage from '../pages/homepage';
import NotFound from '../pages/notfound';
import Login from '../pages/login';
import Logout from '../pages/logout';
import * as constants from '../utils/constants';

const dashboardRoutes = [
  {
    path: constants.PATH_AUTH,
    name: 'Login',
    icon: 'pe-7s-erro',
    component: Login
  },
  {
    path: constants.PATH_LOGOUT,
    name: 'Logout',
    icon: 'pe-7s-graph',
    component: Logout
  },
  {
    path: constants.PATH_HOMEPAGE,
    name: 'Homepage',
    icon: 'pe-7s-graph',
    component: Homepage
  },
  {
    path: constants.PATH_404,
    name: 'Not Found',
    icon: 'pe-7s-erro',
    component: NotFound
  },
  { redirect: true, path: '/', to: constants.PATH_HOMEPAGE, name: 'NotFound' }
];

export default dashboardRoutes;
