
export const formatDateTime = (dateObj) => {
  let [date, time] = new Date(dateObj).toLocaleString('en-US').split(', ');
  return date.concat(' ').concat(time);
};
