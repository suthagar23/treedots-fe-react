import { COOKIE_AUTH } from './constants';
import Cookies from 'js-cookie';

export const CreateAuthCookie = (object) => {
  Cookies.set(COOKIE_AUTH, object, { path: '/', expires: 10 });
  return Cookies.getJSON(COOKIE_AUTH);
};

export const DeleteAuthCookie = () => {
  Cookies.remove(COOKIE_AUTH);
};

export const GetAuthCookie = () => {
  return Cookies.getJSON(COOKIE_AUTH);
};

export function getAuthToken() {
  const { authToken } = GetAuthCookie() || {};
  if (typeof authToken !== undefined ) {
    return authToken;
  }
  return '';
}