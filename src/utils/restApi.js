
import { getAuthToken } from './cookie';

const baseUrl = 'http://localhost:3000/api/v1';

export const fetchPost = (url, payload) => {
  const authToken = getAuthToken();
  return fetch(baseUrl.concat(url), {
    method: 'post',
    headers: {
      Accept: 'application/json, text/plain, */*',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+ authToken, 
    },
    body: JSON.stringify(payload),
  }).then(res => {
    const { status, ok } = res;
    return res.json().then(json => ({ ...json, status, ok }))
  })
    .then(res => res)
    .catch(error => error);
};
