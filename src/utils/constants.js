export const COOKIE_AUTH = 'authCookie';
export const CURRENCY = '$';
export const DISCOUNT = '20';

export const PATH_AUTH = '/auth';
export const PATH_LOGOUT = '/logout';
export const PATH_HOMEPAGE = '/home';
export const PATH_404 = '/notfound';