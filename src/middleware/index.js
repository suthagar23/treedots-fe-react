export function sampleMiddleware({ dispatch }) {
  return (next) => {
    return (action) => {
      return next(action);
    };
  };
}
