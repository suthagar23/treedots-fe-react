
import { combineReducers } from 'redux';
import loginReducer from './pages/login/reducer';;

const rootReducer = combineReducers({
  auth: loginReducer,
});
export default rootReducer;
